package com.PA.API.daoTest;

import com.PA.API.dao.UserDao;
import com.PA.API.dao.UserDaoImpl;
import com.PA.API.model.Login;
import com.PA.API.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class UserDaoTest {

    private DriverManagerDataSource dataSource;
    private UserDao dao;

    @BeforeEach
    void setUpBeforeEach(){
        dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3307/instacode");
        dataSource.setUsername("root");
        dataSource.setPassword("root");

        dao = new UserDaoImpl((javax.sql.DataSource) dataSource);
    }

    @Test
    void testSave(){
        User user = new User(123456,"kévin","jetestencore","testencore@yopmail.com");
        int result = dao.save(user);

        assertTrue(result>0);
    }

    @Test
    void testUpdate(){
        User user = new User(123456,"kévin","jetest","testencore@yopmail.com");
        int result = dao.update(user);

        assertTrue(result>0);
    }

    @Test
    void testGet(){
        int id = 123456;
        User user = dao.get(id);
        assertNotNull(user);
    }

    @Test
    void testDelete(){
        int id = 123456;
        int result = dao.delete(id);

        assertTrue(result>0);
    }

    @Test
    void testList(){
        List<User> users = dao.list();

        /*for (User user:users){
            System.out.println(user);
        }*/

        assertFalse(users.isEmpty());
    }

    @Test
    void testGetUserForLogin(){
        Login login = new Login("olivierpetit@yopmail.fr","jesuislemdp");
        User user = dao.getUserForLogin(login);
        //System.out.println(user.getFirstname());
        assertNotNull(user);
    }
}
