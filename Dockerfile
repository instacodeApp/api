FROM maven:3.6.3-jdk-11

WORKDIR /home/dockerdemo

COPY ./ ./

RUN mvn -f ./pom.xml clean install

ADD /target/*.jar instacode-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/instacode-0.0.1-SNAPSHOT.jar"]