package com.PA.API.daoTest;

import com.PA.API.dao.ResponseDao;
import com.PA.API.dao.ResponseDaoImpl;
import com.PA.API.model.Response;
import com.PA.API.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ResponseDaoTest {
    private DriverManagerDataSource dataSource;
    private ResponseDao dao;

    @BeforeEach
    void setUpBeforeEach(){
        dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3307/instacode");
        dataSource.setUsername("root");
        dataSource.setPassword("root");

        dao = new ResponseDaoImpl((javax.sql.DataSource) dataSource);
    }

    @Test
    void testSave(){
        Date date = new Date();
        Response response = new Response(2,1,"je suis le code","good","",date);
        int result = dao.create(response);

        assertTrue(result>0);
    }

    @Test
    void testUpdate(){
        Date date = new Date();
        Response response = new Response(1,1,"je suis le code modifier","good","",date);
        int result = dao.update(response);

        assertTrue(result>0);
    }

    @Test
    void testDelete(){
        int id = 2;
        int result = dao.delete(id);

        assertTrue(result>0);
    }

    @Test
    void testGet(){
        int id = 1;
        Response response = dao.findById(id);
        assertNotNull(response);
    }

    @Test
    void testListByUserId(){
        int id = 1;
        List<Response> responses = dao.findAllByUserId(id);

        for (Response response:responses){
            System.out.println(response);
        }

        assertFalse(responses.isEmpty());
    }
}
