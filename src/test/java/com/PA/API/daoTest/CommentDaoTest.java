package com.PA.API.daoTest;

import com.PA.API.dao.CommentDao;
import com.PA.API.dao.CommentDaoImpl;
import com.PA.API.model.Comment;
import com.PA.API.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CommentDaoTest {


    private DriverManagerDataSource dataSource;
    private CommentDao dao;

    @BeforeEach
    void setUpBeforeEach(){
        dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3307/instacode");
        dataSource.setUsername("root");
        dataSource.setPassword("root");

        dao = new CommentDaoImpl((javax.sql.DataSource) dataSource);
    }

    @Test
    void testSaveComment(){
        Comment comment = new Comment(1234,5,123,"Kévin","je suis le contenu numéro 2","2022-04-22","2022-04-22");
        int result = dao.saveComment(comment);

        assertTrue(result>0);
    }

    @Test
    void testUpdateComment(){
        Comment comment = new Comment(3,1,123456,"Kévin","je suis le contenu","2022-04-22","2022-04-22");
        int result = dao.updateComment(comment);

        assertTrue(result>0);
    }

    @Test
    void testGetComment(){
        int id = 3;
        Comment comment = dao.getComment(id);
        assertNotNull(comment);
    }

    @Test
    void testDelete(){
        int id = 1;
        int result = dao.deleteComment(id);

        assertTrue(result>0);
    }

    @Test
    void testListComment(){
        List<Comment> comments = dao.listComment();

        /*for (Comment comment:comments){
            System.out.println(comment);
        }*/

        assertFalse(comments.isEmpty());
    }

    @Test
    void testListCommentUser(){
        List<Comment> comments = dao.listCommentUser(123456);

        for (Comment comment:comments){
            System.out.println(comment);
        }

        assertFalse(comments.isEmpty());
    }

    @Test
    void testListCommentByCodeId(){
        List<Comment> comments = dao.listCommentByCodeId(1);

        for (Comment comment:comments){
            System.out.println(comment);
        }

        assertFalse(comments.isEmpty());
    }
}
