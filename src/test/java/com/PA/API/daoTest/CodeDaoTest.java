package com.PA.API.daoTest;

import com.PA.API.dao.CodeDao;
import com.PA.API.dao.CodeDaoImpl;
import com.PA.API.model.Code;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class CodeDaoTest {

    private DriverManagerDataSource dataSource;
    private CodeDao dao;

    @BeforeEach
    void setUpBeforeEach(){
        dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3307/instacode");
        dataSource.setUsername("root");
        dataSource.setPassword("root");

        dao = new CodeDaoImpl((javax.sql.DataSource) dataSource);
    }

    @Test
    void testSaveCode(){
        Code code = new Code(5,123456,"Kévin","System.out.println(je suis le code)","Java","ce code est trop bien",0);
        int result = dao.saveCode(code);

        assertTrue(result>0);
    }

    @Test
    void testUpdateCode(){
        Code code = new Code(1,123456,"Kévin","System.out.println(je suis le code après modification)","Java", "ce code n'est pas ouf",1);
        int result = dao.updateCode(code);

        assertTrue(result>0);
    }

    @Test
    void testGetCode(){
        int id = 1;
        Code code = dao.getCode(id);
        assertNotNull(code);
    }

    @Test
    void testDelete(){
        int id = 1;
        int result = dao.deleteCode(id);

        assertTrue(result>0);
    }

    @Test
    void testListCode(){
        List<Code> codes = dao.listCode();

        /*for (Comment comment:comments){
            System.out.println(comment);
        }*/

        assertFalse(codes.isEmpty());
    }

    @Test
    void testListCodeUser(){
        List<Code> codes = dao.listCodeUser(123456);

        for (Code code:codes){
            System.out.println(code);
        }

        assertFalse(codes.isEmpty());
    }
}
